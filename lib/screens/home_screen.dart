import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task_management_app/screens/all_tasks.dart';

import '../utils/app_colors.dart';
import '../widgets/button_widget.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
                text: "Hello",
                style: TextStyle(
                  color: AppColors.mainColor,
                  fontSize: 60,
                  fontWeight: FontWeight.bold
                ),
                children: [
                  TextSpan(
                  text: "\nstart your beatiful day",
                  style: TextStyle(
                      color: AppColors.smallTextColor,
                      fontSize: 14,

                  ))
                ]
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height/2.5,),
            InkWell(
              onTap: (){
                Get.to(()=>AllTasks(), transition: Transition.zoom, duration: Duration(microseconds: 500));
              },
              child: ButtonWidget(backgroundcolor: AppColors.mainColor,
                  text: "Add Task",
                  textColor: Colors.white),
            ),
            const SizedBox(height: 20,),
            ButtonWidget(backgroundcolor: Colors.white,
                text: "View All",
                textColor: AppColors.smallTextColor)
          ],
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              "assets/welcome.jpg"
            )
          )
        ),
      ),
    );
  }
}
